package com.aero.kmeans;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.aero.kmeans.gui.Item;

//
// K-means Algoritmasi
//
public class Kmeans {
    //
    // Degiskenler, sabitler, sinif uyeleri
    //

    private Cluster[] clusters;

    //
    // Kurucu Metod - Constructor
    //
    public Kmeans(Cluster[] clusters) {
        this.clusters = clusters;
        for (Cluster cluster : this.clusters) {
            cluster.SelectRandomCenter();
        }
    }

    //
    // Iterasyon
    //
    public void Iterate() {
        for (Cluster cluster : this.clusters) {
            cluster.GetCenterElement().SetCoordinate(cluster.CalculateCenter());
        }
    }

    //
    // Kumelerin (renklerin) guncellenmesi
    //
    public void UpdateColors() {
        List<Item> allElements = new ArrayList<Item>();
        for (Cluster cluster : this.clusters) {
            allElements.addAll(cluster.GetElementList());
        }

        for (Item item : allElements) {
            double minDistance = 0;
            for (Cluster cluster : clusters) {
                double distance = distance(item.GetCoordinate(), cluster.GetCenterElement().GetCoordinate());
                if (distance > minDistance) {
                    minDistance = distance;
                    item.GetParentCluster().DeleteElement(item);
                    cluster.AddElement(item);
                    item.SetParentCluster(cluster);
                }
            }
        }
    }

    private double distance(Point p1, Point p2) {
        // TODO calculate distance
        return 0;
    }
}
